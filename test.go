package main

import "fmt"

type hook struct {
	sha string
}

var h = &hook{sha: "xxxx"}

func test(h *hook) {
	fmt.Println(h.sha)
}

func main() {
	h := &hook{sha: "hhh"}
	test(h)
	fmt.Println("vim-go")
}
